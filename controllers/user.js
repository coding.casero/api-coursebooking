//controller
const User = require("../models/User")
const Course = require('../models/Course')
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if the email exists already
/*
	Steps:
	1. "find" mongoose method to find duplicate items
	2. "then" method to send a response back to the FE application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqbody) =>{

	return User.find({email:reqbody.email}).then(result=>{

		//find method returns a record if a match is found
		if(result.length > 0){
			return true;
		}
		//no duplicate found
		//the user is not yet registered in the db
		else{
			return false;
		}


	})
}

//User Registration
/*
	Steps:
	1. create a new user object
	2. make sure that the password is encrypted
	3. save the new User to the database
*/

module.exports.registerUser = (reqbody) =>{

	let newUser = new User({
		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email:reqbody.email,
		mobileNo:reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password,10)
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false;
		}else{
			return true
		}

	})
	.catch(err=>err)
}
//User authentication
/*
	Steps:
	1. check the db if user email exists
	2. compare the password from req.body and password stored in the db
	3. Generate a JWT if the user logged in and return false if not
*/

module.exports.loginUser = (req,res)=>{

	return User.findOne({email:req.body.email}).then(result=>{
		if(result===null){
			return false;
		}else{
			//compareSync method is used to compare a non-encrypted password and from the encrypted password from the db
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
			//if pw match
			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			//else pw does not match
			else{
				return res.send(false);
			}


		}
	})
	.catch(err=>res.send(err))
}

// Retrieve user details
/*
    Steps:
    1. Find the document in the database using the user's ID
    2. Reassign the password of the returned document to an empty string
    3. Return the result back to the frontend
*/
module.exports.getProfile = (req, res) => {

    return User.findById(req.user.id)
    .then(result => {
        result.password = "";
        return res.send(result);
    })
    .catch(err => res.send(err))

};

module.exports.enroll = async(req, res) => {
	//forchecking the in the terminal
	console.log(req.user.id)
	console.log(req.body.courseId)

	//stops admins from getting enrolled in courses
	if (req.user.isAdmin){
		return res.send("Action Forbidden")
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {

		//adding courseID in an objecy and push the obhecty into the users enrollment
		let newEnrollment ={
			courseId: req.body.courseId
		}

		//add the course in the enrollments array
		user.enrollments.push(newEnrollment)

		//save the updated user and return true or error
		return user.save().then(user => true).catch(err => err.message)
	})




	if(isUserUpdated !== true) {
		return res.send({message:isUserUpdated})
	}




	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		
		let enrollee = {
			userId:req.user.id
		}

		course.enrollees.push(enrollee)

		return course.save().then(course => true).catch(err => err.message)
	})


	if(isCourseUpdated !== true){
		return res.send({message:isUserUpdated})
	}


	if(isUserUpdated && isCourseUpdated) {
		return res.send({message:"Enrolled Succesfully!"})
	}

}